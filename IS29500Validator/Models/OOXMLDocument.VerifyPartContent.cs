﻿//License:  New BSD License (BSD)
//Copyright (c) 2010, Jesper Lund Stocholm
//All rights reserved.

//Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

//* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

//* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

//* Neither the name of Jesper Lund Stocholm nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using IS29500Validator.Logging;
using IS29500Validator.Models.Enums;

namespace IS29500Validator.Models
{
    public partial class OOXMLDocument
    {
        /// <summary>
        /// Verifies the content of each part in this document
        /// </summary>
        /// <param name="statusLines_">Status lines</param>
        private void VerifyPartContent(ICollection<StatusLine> statusLines_)
        {
            //StatusLines.Add(new StatusLine() { IsError = false, Message = "Verifying content of all parts" , IsHeadline = true});

            LogMessages.Add(new PartContentLogMessage("Verifying content of all parts"));

            // Get Package
            var package = GetIOPackage();

            // Get all parts
            var partList = package.GetParts();

            foreach (var part in partList)
            {
                //statusLines_.Add(new StatusLine() { IsError = false, Message = String.Format("Checking content of {0} ...", part.Uri) });
                LogMessages.Add(new PartContentLogMessage(String.Format("Checking content of {0} ...", part.Uri)));
                if (!part.ContentType.EndsWith("+xml"))
                {
                    continue;
                }

                XDocument xDoc = XDocument.Load(XmlReader.Create(part.GetStream()));
                //xDoc.Validate(m_xmlSchemaSet, (o, e) => statusLines_.Add(new StatusLine() { IsError = true, Message = e.Message }));
                xDoc.Validate(m_xmlSchemaSet, (o, e) => LogMessages.Add(new PartContentLogMessage(e.Message, Severity.Warning)));

                if (part.ContentType == "application/vnd.openxmlformats-officedocument.extended-properties+xml")
                {
                    this.DocumentGenerator = GetGeneratingApplication(part);
                }
            }
        }
    }
}
