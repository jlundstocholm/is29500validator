﻿//License:  New BSD License (BSD)
//Copyright (c) 2010, Jesper Lund Stocholm
//All rights reserved.

//Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

//* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

//* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

//* Neither the name of Jesper Lund Stocholm nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.IO.Packaging;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using IS29500Validator.Utils.Extensions;

namespace IS29500Validator.Models
{
    public partial class OOXMLDocument
    {
        //Comparison<PackageRelationship> come
        //{
            
        //}

        /// <summary>
        /// Verifies reletionships for this document
        /// </summary>
        /// <param name="statusLines_">Status lines</param>
        private void VerifyRelationshipTypes( ICollection<StatusLine> statusLines_)
        {

            
            statusLines_.Add(new StatusLine() {IsError = false, Message = "Verifying relationshiptypes", IsHeadline = true});

            // Get Package
            var package = GetIOPackage();
            // Get relationships for this package
            var relationships = GetRelationShips(package);

            relationships.Sort((rel1_, rel2_) => rel1_.AbsoluteURI().ToString().CompareTo(rel2_.AbsoluteURI().ToString()));

            foreach (var rel in relationships)
            {
                if (rel.TargetMode == TargetMode.External)
                    continue;

                StatusLine status = new StatusLine() { IsError = false, Message = String.Format("Checking relationshiptype {0} ...", rel.RelationshipType) };
                // Get absolute path in package for each relationship part
                var absoluteUri = PackUriHelper.ResolvePartUri(rel.SourceUri, rel.TargetUri);
                // Get specific part based on absolute path
                var part = this.GetIOPackage().GetPart(absoluteUri);
                // Look up contenttype in Relationship mapping
                if (this.RelationshipMapping.ContainsKey(part.ContentType))
                {
                    var contentRelmap = RelationshipMapping[part.ContentType];
                    // compare relationshiptype in mapping with relationshiptype for part
                    if (contentRelmap.RelationshipType != rel.RelationshipType)
                    {
                        statusLines_.Add(status);
                        statusLines_.Add(new StatusLine() {IsError = true, Message = String.Format("RelationshipType {0} is not valid. It should have been {1}.", rel.RelationshipType, contentRelmap.RelationshipType) });
                    }
                    else
                    {
                        status.Message += " done!";
                        statusLines_.Add(status);
                    }
                }
            }
        }
    }
}
