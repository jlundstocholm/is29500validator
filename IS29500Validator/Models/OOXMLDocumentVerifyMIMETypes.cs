﻿//License:  New BSD License (BSD)
//Copyright (c) 2010, Jesper Lund Stocholm
//All rights reserved.

//Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

//* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

//* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

//* Neither the name of Jesper Lund Stocholm nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace IS29500Validator.Models
{
    public partial class OOXMLDocument
    {
        /// <summary>
        /// Verifies content types for this document
        /// </summary>
        /// <param name="statusLines_"></param>
        private void VerifyContentTypes( ICollection<StatusLine> statusLines_)
        {
            statusLines_.Add(new StatusLine() { IsError = false, Message = "Validating Content-Types.", IsHeadline = true});

            // Get the package
            var package = this.GetIOPackage();

            // Get parts for this package
            var partList = package.GetParts();

            foreach (var part in partList)
            {
                StatusLine status = new StatusLine() {IsError = false, Message = String.Format("Checking {0} ...", part.ContentType)};
                // compare content type in relationship map with content type for this part
                if (part.ContentType.EndsWith("+xml"))
                {
                    if (!this.RelationshipMapping.ContainsKey(part.ContentType))
                    {
                        statusLines_.Add(status);
                        statusLines_.Add(new StatusLine() {IsError = true, Message = string.Format("Content-type {0} is not valid.", part.ContentType)});
                    }
                    else
                    {
                        status.Message += " done!";
                        statusLines_.Add(status);
                    }
                }
            }
        }
    }
}
