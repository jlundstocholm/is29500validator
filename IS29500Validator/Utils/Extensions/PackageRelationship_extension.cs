﻿using System;
using System.Data;
using System.Configuration;
using System.IO.Packaging;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace IS29500Validator.Utils.Extensions
{
    public static class PackageRelationship_extension
    {
        public static Uri AbsoluteURI(this PackageRelationship relationship_)
        {
            return PackUriHelper.ResolvePartUri(relationship_.SourceUri, relationship_.TargetUri);
        }
    }
}
