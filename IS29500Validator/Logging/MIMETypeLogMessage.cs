﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using IS29500Validator.Models.Enums;

namespace IS29500Validator.Logging
{
    public class MIMETypeLogMessage : LogMessage
    {
        public MIMETypeLogMessage(string logMessag_) : base(logMessag_) {}

        public MIMETypeLogMessage(string logMessage, Exception exception_, Severity severity_) : base(logMessage, exception_, severity_) {}
    }
}
