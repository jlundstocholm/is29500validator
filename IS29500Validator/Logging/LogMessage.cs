﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using IS29500Validator.Models.Enums;

namespace IS29500Validator.Logging
{
    public class LogMessage
    {
        Exception m_exception;
        string m_logMessage;
        Severity Severity { get; set; }

        public LogMessage(string logMessage_)
        {
            m_logMessage = logMessage_;
            Severity = Models.Enums.Severity.Information;
        }

        public LogMessage(string logMessage_, Severity severity_) : this(logMessage_)
        {
            Severity = severity_;
        }

        public LogMessage(string logMessage_, Exception exception_, Severity severity_) : this(logMessage_)
        {
            m_exception = exception_;
            Severity = severity_;
        }
    }
}
