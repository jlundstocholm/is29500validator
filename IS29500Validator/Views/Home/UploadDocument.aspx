<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="IS29500Validator.Models"%>
<%@ Import Namespace="IS29500Validator.Controllers"%>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Validator
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<!--
//License:  New BSD License (BSD)
//Copyright (c) 2010, Jesper Lund Stocholm
//All rights reserved.

//Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

//* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

//* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

//* Neither the name of Jesper Lund Stocholm nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->
    <h2>Performing validation</h2>
    <p>
        This service is built entirely on core .Net libraries as well as latest edition of base schemas of IS29500.
    </p>
    <h3>Validate your document here:</h3>
    <form action="/home/uploaddocument" method="post" enctype="multipart/form-data">
    <label for="file">Filename:</label><br />
    <input type="file" name="file" id="file" />
    <br />
    <input type="submit" name="submit" value="Validate" />
    <div style="width:75%">
   
        <% foreach (StatusLine v in (IEnumerable)this.ViewData.Model)
         {%>
             <% if (v.IsHeadline) 
                {%>
                      <h3><%=String.Format("{0}", v.Message)%></h3>
                      
              <%}%>
               
              <% if (v.IsError && !v.IsHeadline) 
                {%>
                      <p style="color:#f00;margin-left:10px;"><%=String.Format("{0}", v.Message)%></p>
                      
              <%}%>
              
              <% if (!v.IsError && !v.IsHeadline) 
                {%>
                      <span style="color:#000;margin-left:10px;"><%=String.Format("{0}", v.Message)%></span><br />
                      
              <%}%>
         <%}%>
         
         <p>Total number of errors: <% = ((IEnumerable<StatusLine>)this.ViewData.Model).Where(x => x.IsError).Count() %></p>
     
      
    </div>

</asp:Content>


