﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>


<asp:Content ID="aboutTitle" ContentPlaceHolderID="TitleContent" runat="server">
    About the validator
</asp:Content>

<asp:Content ID="aboutContent" ContentPlaceHolderID="MainContent" runat="server">
<!--
//License:  New BSD License (BSD)
//Copyright (c) 2010, Jesper Lund Stocholm
//All rights reserved.

//Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

//* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

//* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

//* Neither the name of Jesper Lund Stocholm nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-->
    <h2>About</h2>
    <p>
        To access the source code, please visit  <a href="http://is29500validator.codeplex.com/" title="IS29500 Validator on Codeplex">http://is29500validator.codeplex.com/</a>.
    </p>
    <p>
        To provide feedback as well as report bugs, please visit the <a href="http://is29500validator.codeplex.com/Thread/List.aspx">Discussions</a> or the <a href=http://is29500validator.codeplex.com/WorkItem/List.aspx">work-item list</a>
    </p>
    <p>
        Source code is build on ASP.Net MVC and core .Net libraries and is licensed under a BSD-license.
    </p>
    <p>
        The following data is collected and stored through the validation process:
    </p>
    <ul>
        <li>The current time</li>
        <li>Status of validation</li>
        <li>Generator (application) of the document being validated</li>
        <li>Type of document (text-document, presentation or spreadsheet)</li>
        <li>Time the document was created</li>
        <li>Your IP-adress</li>
    </ul>
    <p>Documents are validated against the latest up-to-date version of the base schemas of ISO/IEC 29500. At present (February 2010) these are initial schemas plus corrections from the COR1 set of documents.</p>
    <h2>This is NOT an official validation tool.</h2>
</asp:Content>
