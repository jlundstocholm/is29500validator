﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Routing;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using IS29500Validator.Models;

namespace IS29500Validator
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default",                                              // Route name
                "{controller}/{action}/{id}",                           // URL with parameters
                new { controller = "Home", action = "Index", id = "" }  // Parameter defaults
            );

        }

        protected void Session_Start()
        {
            // Get OOXML Schema SchemaSet from cache
            var xmlSchemaSet = HttpRuntime.Cache["ooxmlschemas"] as XmlSchemaSet;

            // If it has not been loaded to cache, load it to cache.
            if (xmlSchemaSet == null)
                LoadSchemaSet();
        }

        protected void Application_Start()
        {
            RegisterRoutes(RouteTable.Routes);

            LoadXMLMapping();
            LoadSchemaSet();

        }

        void LoadSchemaSet()
        {
            var xmlSchemaSet = Utils.Xml.LoadSchemaSet();

            HttpContext.Current.Cache.Add("ooxmlschemas", xmlSchemaSet, null, DateTime.Now.AddHours(24), TimeSpan.Zero, CacheItemPriority.Normal, null);
        }

        void LoadXMLMapping()
        {
            var dictionary = Utils.Xml.LoadXmlMapping();

            HttpContext.Current.Cache.Add("ooxmlmap", dictionary, null, DateTime.Now.AddHours(24), TimeSpan.Zero, CacheItemPriority.Normal, null);
        }
    }
}